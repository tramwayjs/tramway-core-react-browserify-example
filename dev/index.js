import ReactDOM from 'react-dom';
import {Router} from 'tramway-core-router';
import RouterStrategy, {components} from 'tramway-router-react-strategy';
let {ReactRouter} = components;

import routes from './routes/routes.js';

let InitializedRouter = new Router(routes, new RouterStrategy(ReactRouter));
let IR = InitializedRouter.initialize();

ReactDOM.render(
  IR,
  document.getElementById('root')
);
