import React from 'react';
import {controllers} from 'tramway-router-react-strategy';
import App from '../components/App.jsx';

let {NotFoundController} = controllers;

export default class PageNotFoundController extends NotFoundController {
    render() {
        return <App message="The page you requested could not be found" />;
    }
}