import React from 'react';
import {controllers} from 'tramway-router-react-strategy';
import App from '../components/App.jsx';

let {ReactController} = controllers;

export default class HelloController extends ReactController {
    render() {
        return <App message={`Hello ${this.params.name || "world"}`} />;
    }
}