import React from 'react';
import {controllers} from 'tramway-router-react-strategy';
import App from '../components/App.jsx';
import os from 'os';

let {ReactController} = controllers;

/**
 * @class MainController
 * @extends {Controller}
 */
export default class MainController extends ReactController {
    render() {
        return <App message={`Page rendered from ${os.hostname()}`} />;
    }
}