import {Connection} from 'tramway-core';

export default class TestConnection extends Connection {
    /**
     * @param {Object} params
     */
    constructor(params){
        super();
    }

   /**
     * @param {number|string} id
     * @param {function(Error, Object)} cb
     * 
     * @memberOf Connection
     */
    getItem(id, cb) {
        if (0 === id) {
            return cb(new Error('Not found'));
        }
        return cb(null, {id: id});
    }

    /**
     * @param {string[] | number[]} ids
     * @param {function(Error, Object[])} cb
     * 
     * @memberOf Connection
     */
    getItems(ids, cb) {
        if (0 === ids.length) {
            return cb(new Error('Not found'));
        }
        return cb(null, ids.map(function(id) {
            return {id: id};
        }));
    }

    /**
     * @param {function(Error, boolean)} cb
     * 
     * @memberOf Connection
     */
    getAllItems(cb) {
        return cb(null, [1,2,3,4].map(function(id) {
            return {id: id};
        }));
    }

    /**
     * @param {string | Object} conditions
     * @param {function(Error, Object[])} cb
     * 
     * @memberOf Connection
     */
    findItems(conditions, cb) {
        return cb(null, [conditions]);
    }

    /**
     * @param {number|string} id
     * @param {function(Error, boolean)} cb
     * 
     * @memberOf Connection
     */
    hasItem(id, cb) {

    }

    /**
     * @param { string[] | number[] } ids
     * @param {function(Error, boolean)} cb
     * 
     * @memberOf Connection
     */
    hasItems(ids, cb) {

    }

    /**
     * @param {string | Object} conditions
     * @param {function(Error, number)} cb
     * 
     * @memberOf Connection
     */
    countItems(conditions, cb) {

    }

    /**
     * @param {Object} item
     * @param {function(Error, Object)} cb
     * 
     * @memberOf Connection
     */
    createItem(item, cb) {
        if (null === item || "function" === typeof item) {
            return cb(new Error('Invalid request'));
        }
        return cb(null, {id: 100});
    }

    /**
     * @param {number|string} id
     * @param {Object} item
     * @param {function(Error, Object)} cb
     * 
     * @memberOf Connection
     */
    updateItem(id, item, cb) {
        if (0 === id || null === item) {
            return cb(new Error("Invalid request"));
        }
        return cb(null, item);
    }

    /**
     * @param {number|string} id
     * @param {function(Error, Object)} cb
     * 
     * @memberOf Connection
     */
    deleteItem(id, cb) {
        return cb(null, {id: id});
    }

    /**
     * @param { number[] | string[]} id
     * @param {function(Error, Object[])} cb
     * 
     * @memberOf Connection
     */
    deleteItems(id, cb) {

    }

    /**
     * Recommended to use other functions first.
     * @param {string} query
     * @param {[] | Object} values
     * @param {function(Error, Object[])} cb
     * 
     * @memberOf Connection
     */
    query(query, values, cb) {

    }
}