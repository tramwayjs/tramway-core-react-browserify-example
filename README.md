[![build status](https://gitlab.com/tramwayjs/tramway-router-react-strategy/badges/master/build.svg)](https://gitlab.com/tramwayjs/tramway-router-react-strategy/commits/master)

[![coverage report](https://gitlab.com/tramwayjs/tramway-router-react-strategy/badges/master/coverage.svg)](https://gitlab.com/tramwayjs/tramway-router-react-strategy/commits/master)

This repo gives a demo of how to use [Tramway-Router-React-Strategy](https://gitlab.com/tramwayjs/tramway-router-react-strategy) to build a "server-less" app using gulp.

# Set up instructions
1. Install gulp globally `npm install --global gulp-cli`
2. Run `npm install`

# To use

To build:
```
npm build
```

To start:
```
npm start
```

The page that will run will be accessible via http://localhost:3000 by default.