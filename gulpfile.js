"use strict";

const gulp = require('gulp');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const livereload = require('gulp-livereload');
var connect = require('gulp-connect');
var history = require('connect-history-api-fallback');

const DEV_DIRECTORY = 'dev';
const DIST_DIRECTORY = 'dist';

gulp.task('default', ['bundle'], function(){
    connect.server({
        root: ['dist'],
        port: 3000,
        base: '/',
        livereload: true,
        middleware: function(connect, opt) {
            return [
                history({})
            ]
        }
    });
});

gulp.task('browserify', function() {
    return browserify({
        extensions: ['.js', '.jsx'],
        entries: DEV_DIRECTORY + '/index.js'
    })
    .transform(babelify.configure({
        "ignore": /(bower_components)|(node_modules)/,
        "plugins": [
            "transform-flow-strip-types",
            "transform-react-jsx"
        ],
        "presets": [
            "modern-browsers",
            "import-export",
            "react"
        ]
    }))
    .bundle()
    .on("error", function (err) {console.log(err)})
    .pipe(source('index.js'))
    .pipe(gulp.dest(DIST_DIRECTORY));
});

gulp.task('bundle', ['browserify', 'buildViews']);


gulp.task('buildViews', function(){
    return gulp
        .src(DEV_DIRECTORY + '/**/*.html')
        .pipe(gulp.dest(DIST_DIRECTORY))
        .pipe(livereload());
});

gulp.task('watch', ['bundle'], function(){
    livereload.listen();
    gulp.watch(DEV_DIRECTORY, ['bundle']);
});